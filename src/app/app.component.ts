import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';

  animals=[
    {
      name:'Loutre',
      img:'/assets/loutre.jpg',
      type:'Mustelidés'
    },
    {
      name:'Loup',
      img:'/assets/loup.jpg',
      type:'Canidé'
    },
    {
      name:'Lynx roux',
      img:'/assets/lynx.jpg',
      type:'Félin'
    }
  ]

  SelectedAnimal:{
    name: string,
    img: string,
    type: string
  } = {
    name:'',
    img: '',
    type:''
  }

  selectAnimals(animals){
    this.SelectedAnimal.name = animals.name;
    this.SelectedAnimal.img = animals.img;
    this.SelectedAnimal.type = animals.type;
    return this.SelectedAnimal;
  }
  
}
